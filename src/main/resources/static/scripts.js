var stompClient = null;

$(document).ready(function () {
    console.log("Index page is ready");
    connect();

    $("#send-private").click(function () {
        if (alertMessage())
            sendPrivateMessage();
    });

    $("#who-am-i").click(function () {
        showUser();
    });


});

function alertMessage() {
    if ($("#sessionId").val().length === 0) {
        alert("Click button - Who am I?")
        return false;
    }
    return true;
}

function connect() {
    var socket = new SockJS('/our-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);

        stompClient.subscribe('/user/topic/private-messages', function (message) {
            showPrivateMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/user/topic/i-am', function (user) {
            showWhoIAm(JSON.parse(user.body))
        })

    });
}

function showUser(user) {
    stompClient.send("/ws/who-i-am", {}, "");
    document.getElementById("sessionId").value = user.sessionId;
}

function showWhoIAm(user) {
    document.getElementById("sessionId").value = user.sessionId;
}


function showPrivateMessage(privateMessage) {
    $("#privateMessage").append("<tr><td>" + "<b>received from :</b>", privateMessage.senderId, " :  ", privateMessage.messageContent + "</td></tr>");
}

function sendPrivateMessage() {
    let messageId = () => {
        return Math.floor((1 + Math.random()) * 0x110000)
            .toString(16)
            .substring(1);
    }
    console.log("sending private message");
    stompClient.send("/ws/private-chatMessage", {}, JSON.stringify({
        'id': messageId(),
        'messageContent': $("#private-chatMessage").val(),
        'recipientId': $("#send-to-chatMessage").val(),
        'senderId': $("#sessionId").val(),
        'timestamp': Date.now()
    }));
    $("#privateMessage").append("<tr><td>" + "<b>sended to:</b>", $("#send-to-chatMessage").val(), " :  ", $("#private-chatMessage").val() + "</td></tr>");
    document.getElementById("private-chatMessage").value = "";
}

