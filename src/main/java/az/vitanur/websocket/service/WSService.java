package az.vitanur.websocket.service;

import az.vitanur.websocket.model.ChatMessage;
import az.vitanur.websocket.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class WSService {

    private final SimpMessagingTemplate messagingTemplate;

    @Autowired
    public WSService(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    public void notifyUser(ChatMessage chatMessage) {

        messagingTemplate.convertAndSendToUser(chatMessage.getRecipientId(), "/topic/private-messages", chatMessage);
    }

    public void getUser(Principal principal) {
        User user = new User(principal.getName());
        messagingTemplate.convertAndSendToUser(user.getSessionId(), "/topic/i-am", user);
    }
}
