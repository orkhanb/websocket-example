package az.vitanur.websocket.controller;

import az.vitanur.websocket.model.ChatMessage;
import az.vitanur.websocket.service.WSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WSController {

    @Autowired
    private WSService service;


    @PostMapping("/send-private-message")
    public void sendPrivateMessage(@RequestBody ChatMessage chatMessage) {
        service.notifyUser(chatMessage);
    }
}
