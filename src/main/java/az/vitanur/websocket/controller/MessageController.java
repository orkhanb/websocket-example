package az.vitanur.websocket.controller;

import az.vitanur.websocket.model.ChatMessage;
import az.vitanur.websocket.service.WSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class MessageController {
    @Autowired
    private WSService wsService;

    @MessageMapping("/private-chatMessage")
    @SendToUser("/topic/private-messages")
    public void getPrivateMessage(@RequestBody ChatMessage chatMessage) {
        wsService.notifyUser(chatMessage);
    }

    @MessageMapping("who-i-am")
    @SendToUser("topic/i-am")
    public void getUserBySessionId(final Principal principal) {
        wsService.getUser(principal);
    }
}
